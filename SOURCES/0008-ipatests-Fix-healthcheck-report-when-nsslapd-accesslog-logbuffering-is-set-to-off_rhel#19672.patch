From 5afda72afc6fd626359411b55f092989fdd7d82d Mon Sep 17 00:00:00 2001
From: Rob Crittenden <rcritten@redhat.com>
Date: Jan 15 2024 13:39:21 +0000
Subject: ipatests: ignore nsslapd-accesslog-logbuffering WARN in healthcheck


Log buffering is disabled in the integration tests so we can have all
the logs at the end. This is causing a warning to show in the 389-ds
checks and causing tests to fail that expect all SUCCESS.

Add an exclude for this specific key so tests will pass again.

We may eventually want a more sophisiticated mechanism to handle
excludes, or updating the config in general, but this is fine for now.

Fixes: https://pagure.io/freeipa/issue/9400

Signed-off-by: Rob Crittenden <rcritten@redhat.com>
Reviewed-By: Florence Blanc-Renaud <frenaud@redhat.com>
Reviewed-By: Michal Polovka <mpolovka@redhat.com>
Reviewed-By: Florence Blanc-Renaud <frenaud@redhat.com>
Reviewed-By: Michal Polovka <mpolovka@redhat.com>

---

diff --git a/ipatests/test_integration/test_ipahealthcheck.py b/ipatests/test_integration/test_ipahealthcheck.py
index 7fb8e40..14fba26 100644
--- a/ipatests/test_integration/test_ipahealthcheck.py
+++ b/ipatests/test_integration/test_ipahealthcheck.py
@@ -9,6 +9,7 @@ from __future__ import absolute_import
 
 from configparser import RawConfigParser, NoOptionError
 from datetime import datetime, timedelta
+import io
 import json
 import os
 import re
@@ -208,6 +209,28 @@ def run_healthcheck(host, source=None, check=None, output_type="json",
     return result.returncode, data
 
 
+def set_excludes(host, option, value,
+                 config_file='/etc/ipahealthcheck/ipahealthcheck.conf'):
+    """Mark checks that should be excluded from the results
+
+       This will set in the [excludes] section on host:
+           option=value
+    """
+    EXCLUDES = "excludes"
+
+    conf = host.get_file_contents(config_file, encoding='utf-8')
+    cfg = RawConfigParser()
+    cfg.read_string(conf)
+    if not cfg.has_section(EXCLUDES):
+        cfg.add_section(EXCLUDES)
+    if not cfg.has_option(EXCLUDES, option):
+        cfg.set(EXCLUDES, option, value)
+    out = io.StringIO()
+    cfg.write(out)
+    out.seek(0)
+    host.put_file_contents(config_file, out.read())
+
+
 @pytest.fixture
 def restart_service():
     """Shut down and restart a service as a fixture"""
@@ -265,6 +288,7 @@ class TestIpaHealthCheck(IntegrationTest):
             setup_dns=True,
             extra_args=['--no-dnssec-validation']
         )
+        set_excludes(cls.master, "key", "DSCLE0004")
 
     def test_ipa_healthcheck_install_on_master(self):
         """
@@ -552,6 +576,7 @@ class TestIpaHealthCheck(IntegrationTest):
                               setup_dns=True,
                               extra_args=['--no-dnssec-validation']
                               )
+        set_excludes(self.replicas[0], "key", "DSCLE0004")
 
         # Init a user on replica to assign a DNA range
         tasks.kinit_admin(self.replicas[0])
@@ -692,6 +717,7 @@ class TestIpaHealthCheck(IntegrationTest):
                 'output_type=human'
             ])
         )
+        set_excludes(self.master, "key", "DSCLE0004", config_file)
         returncode, output = run_healthcheck(
             self.master, failures_only=True, config=config_file
         )
@@ -707,6 +733,7 @@ class TestIpaHealthCheck(IntegrationTest):
                 'output_file=%s' % HC_LOG,
             ])
         )
+        set_excludes(self.master, "key", "DSCLE0004")
         returncode, _unused = run_healthcheck(
             self.master, config=config_file
         )
@@ -2396,6 +2423,7 @@ class TestIpaHealthCLI(IntegrationTest):
             cls.master, setup_dns=True, extra_args=['--no-dnssec-validation']
         )
         tasks.install_packages(cls.master, HEALTHCHECK_PKG)
+        set_excludes(cls.master, "key", "DSCLE0004")
 
     def test_indent(self):
         """
diff --git a/ipatests/test_integration/test_replica_promotion.py b/ipatests/test_integration/test_replica_promotion.py
index d477c3a..b71f2d5 100644
--- a/ipatests/test_integration/test_replica_promotion.py
+++ b/ipatests/test_integration/test_replica_promotion.py
@@ -13,7 +13,7 @@ import pytest
 
 from ipatests.test_integration.base import IntegrationTest
 from ipatests.test_integration.test_ipahealthcheck import (
-    run_healthcheck, HEALTHCHECK_PKG
+    run_healthcheck, set_excludes, HEALTHCHECK_PKG
 )
 from ipatests.pytest_ipa.integration import tasks
 from ipatests.pytest_ipa.integration.tasks import (
@@ -983,6 +983,9 @@ class TestHiddenReplicaPromotion(IntegrationTest):
         # manually install KRA to verify that hidden state is synced
         tasks.install_kra(cls.replicas[0])
 
+        set_excludes(cls.master, "key", "DSCLE0004")
+        set_excludes(cls.replicas[0], "key", "DSCLE0004")
+
     def _check_dnsrecords(self, hosts_expected, hosts_unexpected=()):
         domain = DNSName(self.master.domain.name).make_absolute()
         rset = [

From f1cfe7d9ff2489dbb6cad70999b0e1bd433c0537 Mon Sep 17 00:00:00 2001
From: Rob Crittenden <rcritten@redhat.com>
Date: Jan 15 2024 13:39:21 +0000
Subject: ipatests: fix expected output for ipahealthcheck.ipa.host


ipa-healthcheck commit e69589d5 changed the output when a service
keytab is missing to not report the GSSAPI error but to report
that the keytab doesn't exist at all. This distinguishes from real
Kerberos issues like kvno.

Fixes: https://pagure.io/freeipa/issue/9482

Signed-off-by: Rob Crittenden <rcritten@redhat.com>
Reviewed-By: Florence Blanc-Renaud <frenaud@redhat.com>
Reviewed-By: Michal Polovka <mpolovka@redhat.com>
Reviewed-By: Florence Blanc-Renaud <frenaud@redhat.com>
Reviewed-By: Michal Polovka <mpolovka@redhat.com>

---

diff --git a/ipatests/test_integration/test_ipahealthcheck.py b/ipatests/test_integration/test_ipahealthcheck.py
index 14fba26..8aae9fa 100644
--- a/ipatests/test_integration/test_ipahealthcheck.py
+++ b/ipatests/test_integration/test_ipahealthcheck.py
@@ -629,9 +629,15 @@ class TestIpaHealthCheck(IntegrationTest):
         ipahealthcheck.ipa.host when GSSAPI credentials cannot be obtained
         from host's keytab.
         """
-        msg = (
-            "Minor (2529639107): No credentials cache found"
-        )
+        version = tasks.get_healthcheck_version(self.master)
+        if parse_version(version) >= parse_version("0.15"):
+            msg = (
+                "Service {service} keytab {path} does not exist."
+            )
+        else:
+            msg = (
+                "Minor (2529639107): No credentials cache found"
+            )
 
         with tasks.FileBackup(self.master, paths.KRB5_KEYTAB):
             self.master.run_command(["rm", "-f", paths.KRB5_KEYTAB])

