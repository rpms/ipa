From 84798137fabf75fe79aebbd97e4b8418de8ab0f2 Mon Sep 17 00:00:00 2001
From: Francisco Trivino <ftrivino@redhat.com>
Date: Fri, 19 Jan 2024 18:15:28 +0100
Subject: [PATCH] Vault: improve vault server archival/retrieval calls
 error handling

If a vault operation fails, the error message just says "InternalError". This commit
improves error handling of key archival and retrieval calls by catching the PKIException
error and raising it as an IPA error.

Related: https://pagure.io/freeipa/issue/9191

Signed-off-by: Francisco Trivino <ftrivino@redhat.com>
Reviewed-By: Rob Crittenden <rcritten@redhat.com>
(cherry picked from commit dc1ab53f0aa0398d493f7440b5ec4d70d9c7d663)
---
 ipaserver/plugins/vault.py | 40 +++++++++++++++++++++++++-------------
 1 file changed, 26 insertions(+), 14 deletions(-)

diff --git a/ipaserver/plugins/vault.py b/ipaserver/plugins/vault.py
index 574c83a9a..13c4fac9a 100644
--- a/ipaserver/plugins/vault.py
+++ b/ipaserver/plugins/vault.py
@@ -45,6 +45,7 @@ if api.env.in_server:
     import pki.key
     from pki.crypto import DES_EDE3_CBC_OID
     from pki.crypto import AES_128_CBC_OID
+    from pki import PKIException
 
 if six.PY3:
     unicode = str
@@ -1094,16 +1095,21 @@ class vault_archive_internal(PKQuery):
                     pki.key.KeyClient.KEY_STATUS_INACTIVE)
 
             # forward wrapped data to KRA
-            kra_client.keys.archive_encrypted_data(
-                client_key_id,
-                pki.key.KeyClient.PASS_PHRASE_TYPE,
-                wrapped_vault_data,
-                wrapped_session_key,
-                algorithm_oid=algorithm_oid,
-                nonce_iv=nonce,
-            )
-
-            kra_account.logout()
+            try:
+                kra_client.keys.archive_encrypted_data(
+                    client_key_id,
+                    pki.key.KeyClient.PASS_PHRASE_TYPE,
+                    wrapped_vault_data,
+                    wrapped_session_key,
+                    algorithm_oid=algorithm_oid,
+                    nonce_iv=nonce,
+                )
+            except PKIException as e:
+                kra_account.logout()
+                raise errors.EncodingError(
+                    message=_("Unable to archive key: %s") % e)
+            finally:
+                kra_account.logout()
 
         response = {
             'value': args[-1],
@@ -1174,11 +1180,17 @@ class vault_retrieve_internal(PKQuery):
             kra_client.keys.encrypt_alg_oid = algorithm_oid
 
             # retrieve encrypted data from KRA
-            key = kra_client.keys.retrieve_key(
-                key_info.get_key_id(),
-                wrapped_session_key)
+            try:
 
-            kra_account.logout()
+                key = kra_client.keys.retrieve_key(
+                    key_info.get_key_id(),
+                    wrapped_session_key)
+            except PKIException as e:
+                kra_account.logout()
+                raise errors.EncodingError(
+                    message=_("Unable to retrieve key: %s") % e)
+            finally:
+                kra_account.logout()
 
         response = {
             'value': args[-1],
-- 
2.43.0

